
//Write down a program to store the details of students using the knowledge of loops, arrays and structures
#include<stdio.h>
struct student{
    char name[40];
    char subject[40];
    float marks;
};

int main()
{
   struct student arr[10];
   int x;
   for (x=1;x<=5;x++)
   {
       printf("Enter Student's first Name:");
       scanf("%s",arr[x].name);
       printf("Enter subject:");
       scanf("%s",arr[x].subject);
       printf("Enter marks:");
       scanf("%f",&arr[x].marks);
   }
   printf("Student details\n");

      for (x=1;x<=5;x++)
   {
       printf("%d.First name of the student=%s\nSubject=%s\nMarks=%0.2f\n",x,arr[x].name,arr[x].subject,arr[x].marks);
       printf("\n");
   }
   return 0;

}

